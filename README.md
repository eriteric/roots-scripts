# Roots Scripts
A collection of bash scripts for speeding up work with Roots.io WordPress tools

## requirements
* [ jq ]( https://stedolan.github.io/jq/ )
* [ just ]( https://just.systems/man/en/chapter_1.html )
* [ sponge ]( https://linux.die.net/man/1/sponge )

## newsage10
Fairly opinionated script for adding a new [Roots Sage 10 theme]( https://roots.io/sage ) to an existing [Roots Bedrock](https://roots.io/bedrock) installation.

### Instructions
1. This script should be run in the Bedrock root directory.
2. Requires one argument: a project name/acronym (designed for one word no spaces)

example: `newsage10 abc`

### Details
* Choose framework: Tailwind or Bootstrap
* Copies in starter files from templates directory
* Copies in helper scripts for working on Sage 10
* Creates a .tool-versions file for use with asdf-vm or other version managers
* Adds debugging for critical errors in Acorn (no more blank screen)
* CSS for navigation header that changes while scrolling (plan to add in JS later)
* Installs:
  + Acorn (installed to Bedrock's composer.json)
  + Blade-icons (installed to Bedrock's composer.json)
  + ACF Composer
  + ACF Builder
  + Sage Directives
  + Boostrap 5 nav walker

## Commands
* `just refresh`
  + clear out all laravel cache
  + flush permalinks
  + yarn build
  + audio tone on completion (you'll need to assign this file yourself)
* `just dev`
  + yarn dev
* `just build`
  + yarn build
  + audio tone on completion (you'll need to assign this file yourself)

## newflex
Adds a new ACF flex module and then opens all files in nvim (change editor to your liking).

## updatebedrock
Updates an existing bedrock installation. After running, you can diff composer.json and composer-old.json to see what needs to be copied over from your old install.

## zipsage10
Prepares a sage10 theme for delivery to a panel hosting environment. Meant to be run from inside the web/app/themes directory.

## install_wpcli_packages
Install wp-cli packages on the server. Takes one optional argument: wp-cli environment alias.

example: `install_wpcli_packages @dev`
