refresh:
  wp @dev rewrite flush
  wp @dev acorn optimize:clear
  wp @dev acorn view:cache
  wp @dev acorn icons:cache
  yarn build
  mpv ~/music/sounds/mixkit-epic-orchestra-transition-2290.wav

dev:
  yarn dev

build:
  yarn build
  mpv ~/music/sounds/mixkit-epic-orchestra-transition-2290.wav
