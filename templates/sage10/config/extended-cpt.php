<?php
add_action( 'init', function() {
/**
 * Register a custom post type
 *
 * https://github.com/johnbillion/extended-cpts/wiki/Registering-Post-Types
 * 
 */

// register post type - simple:
//
//add_action( 'init', function() {
//	register_extended_post_type( 'article' );
//} );
//
//
// register post type - with options:
//
//	register_extended_post_type( 'podcast', [
//		# Add the post type to the site's main RSS feed:
//		'show_in_feed' => true,
//    'show_ui' => true,
//    'supports' => ['title', 'editor', 'excerpt', 'author', 'revisions', 'thumbnail', 'custom-fields'],
//    'menu_icon' => 'dashicons-playlist-audio',
//    'show_in_rest' => false,
//    'has_archive' => true,
//    'public' => true,
//    'publicly_queryable' => true,
//	  ], [
//		# Override the base names used for labels:
//		'singular' => 'Podcast',
//		'plural'   => 'Podcasts',
//		'slug'     => 'podcasts',
//	] );



/**
 * Register a custom taxonomy
 *
 * https://github.com/johnbillion/extended-cpts/wiki/Registering-taxonomies
 * 
 */
// register taxonomy - simple:
//
//  register_extended_taxonomy( 'location', 'post' );
//
//
// register taxonomy - with options:
//
//register_extended_taxonomy( 'genre', 'story', array(
//	# Use radio buttons in the meta box for this taxonomy on the post editing screen:
//	'meta_box' => 'radio',
//
//	# Show this taxonomy in the 'At a Glance' dashboard widget:
//	'dashboard_glance' => true,
//
//	# Add a custom column to the admin screen:
//	'admin_cols' => array(
//		'updated' => array(
//			'title'       => 'Updated',
//			'meta_key'    => 'updated_date',
//			'date_format' => 'd/m/Y'
//		),
//	),
//
//), array(
//
//	# Override the base names used for labels:
//	'singular' => 'Genre',
//	'plural'   => 'Genres',
//	'slug'     => 'story-genre'
//
//) );


} );
